var expect = require('chai').expect
	, mongoose = require('mongoose')
	, service = require('../src/server/services/user')
;

describe('User', function(){

	before(function (done) {		
		mongoose.connect('mongodb://localhost/mochatest');
		
		mongoose.connection.on('connected', function(){
			console.log('successfully connected to mongodb');			
						
			done();
		});		
	})
	
	after(function(done){
		mongoose.disconnect(done());
	})
	
	var testUser = {
		name:{
			first:'firstName',
			middle:'middleName',
			last:'lastName'
		},
		email:'test@test.com'
	}
	
	describe('#create()', function(){
		it('should create a new test user', function(done){				
			service.create(testUser, null, function(err, user){
				console.log('new user created');
				testUser = user;
				done(err, user);
			});			
		})
	})
  
})





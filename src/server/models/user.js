var mongoose = require('mongoose')
	, Schema = mongoose.Schema
;

var userSchema = new Schema({
  name:{
		first:String,
		last:String,
		middle:String
	},
	email:String
  
});

module.exports = User = mongoose.model('User', userSchema);
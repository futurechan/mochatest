var User = require('../models/user')
	, mongoose = require('mongoose')
	, ObjectId = mongoose.Types.ObjectId
;


var setup=function(app) {

};

var find = function(params, callback) {
	User.find(params, callback);
}

var get =  function(id, params, callback) {
	User.findById(new ObjectId(id), callback);
}

var create = function(data, params, callback) {	
	console.log('creating new user: ' + data);
	User.create(data, callback);
}

var update = function(id, data, params, callback){
	User.update({_id:new ObjectId(id)}, data, {}, callback);
}

var remove = function(id, params, callback) {
	User.findByIdAndRemove(new ObjectId(id), callback);
}


module.exports={
  find: find,
  get:get,
  create: create,
  update: update,
  //patch: function(id, data, params, callback) {},
  remove: remove,
  setup: setup
}
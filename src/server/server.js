var feathers = require('feathers')
	, path = require('path')
	, app = feathers({ rest: true })
	, clientDir = path.join(__dirname, '../client')
	, mongoose = require('mongoose')
;

app.use(feathers.static(clientDir));


app.use('/user', require('./services/user'))
;

app.use(function(req,res,next){
  res.format({
		html: function(){
		  res.sendfile(path.join(clientDir, 'index.html'));
		},
		json: function(){
		  next();
		}
	})	
});

app.configure(feathers.socketio());

mongoose.connect('localhost', 'mochatest');

app.listen(8080);
  
  
  
